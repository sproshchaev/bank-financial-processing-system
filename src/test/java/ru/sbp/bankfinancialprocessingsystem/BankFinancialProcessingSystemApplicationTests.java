package ru.sbp.bankfinancialprocessingsystem;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(classes = ru.sbp.bankfinancialprocessingsystem.TestAppConfiguration.class)
class BankFinancialProcessingSystemApplicationTests {

	@Test //
	void contextLoads() {
	}

}
